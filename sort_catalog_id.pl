#!/usr/bin/perl

############################################################
#                                                          #
# Script sorts the catalog Ids and regenarates sort keys   #
# It queries the corpus table to check for any new corpus  #
# additions. If there are new corpus additions then it     #
# sorts the catalog ids and reassigns sort key numbers.    #
#                                                          #
# The front end jsp queries the database and orders        #
#  by sort_key attribute.                                  # 
#                                                          #
############################################################


use strict;
use lib "/mdc/lib/perl5/site_perl";
use MysqlLex;

my @sorted_array;

# DATABASE INFORMATION
my $db=MysqlLex->new("bizsys");

my $count=$db->queryCell("SELECT count(mdc_catalog_id) from corpus");

my $last_count=0;

# READ FILE TO SEE IF THE NUMBER OF ROWS HAVE CHANGED
open (MYFILE, '/mdc/home/mendonca/count.txt');
while (<MYFILE>) 
{
    chomp;
    $last_count=$_;
}
close (MYFILE); 

warn "Count file does not exist" unless $last_count;

# IF THERE IS A CHANGE IN THE NUMBER OF ROWS THEN EXECUTE THE IF BLOCK
# ELSE IT WILL CONTINUE RESULTING IN TERMINATION OF THE SCRIPT.

if($count!=$last_count)
{
    my $cids= $db->queryColumn("SELECT mdc_catalog_id from corpus");

    foreach(sort mysort @$cids) {
	push(@sorted_array,$_);

    }

    my $index=0;
    my $string= "update corpus set sort_key=?  where mdc_catalog_id=?";
    my $pupd = $db->sqlPrep($string);

    for my $id(@sorted_array) {
	# print "$id\n";
	$db->sqlApply($pupd,++$index,$id);
    }
    open (MYFILE, '>/mdc/home/mendonca/count.txt');
    print MYFILE "$count\n";
    close (MYFILE); 

}# END OF IF BLOCK


# SORT SUBROUTINE.
sub mysort
{
    my ($year1,$type1,$index1,$sub1) = ($a=~/mdc(\d+)([A-Z])(\d+)(.*)/);
    my ($year2,$type2,$index2,$sub2) = ($b=~/mdc(\d+)([A-Z])(\d+)(.*)/);

    ( $year1 <=> $year2 ) || ( $type1 cmp $type2 ) || 
	( $index1 <=> $index2 ) || ( $sub1 cmp $sub2 );
}

# END OF CODE
